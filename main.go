package main

import(
	"os"
	"fmt"
	"time"
	"strings"
	"encoding/json"
	"io/ioutil"
)

var text string

func write_files(){
    f, err := os.Create("out.txt")
    if err != nil {
	    fmt.Println(err)
	    f.Close()
	    return
    }

    d := []string{text}

    for _, v := range d {
	    fmt.Fprintln(f, v)
	    if err != nil {
		fmt.Println(err)
            }
    }

    err = f.Close()
    if err != nil {
	    fmt.Println(err)
    }

    fmt.Println("file was written line by line successfully")
}

func read_files(){
       fileInfo, err := ioutil.ReadDir("rules")
       if err != nil {}
       for _, file := range fileInfo {
	       converter("rules/" + file.Name())
       }
       fmt.Println(text)
       write_files()
}

func converter(filename string){
	file, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println(err)
	}

	c := make(map[string]string)
	e := json.Unmarshal(file, &c)

	 if e != nil {
		panic(e)
		fmt.Println(e)
	 }

	var key, value string

	for key, value = range c {
		text = strings.Replace(string(text), key, value, -1)
	}
}

func main(){
	text_file, err := ioutil.ReadFile("text.txt")
	if err != nil {
		fmt.Println(err)
	}

	text = string(text_file)
	read_files()
}
